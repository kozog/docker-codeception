<?php
namespace Helper;

use Symfony\Component\Process\Process;

class Acceptance extends \Codeception\Module
{
    private $recorder;
    private $saveVideos = [];

    public function _before(\Codeception\TestInterface $test)
    {
        $this->debug($this->getCommand($test));
        $this->recorder = new Process($this->getCommand($test));
        $this->recorder->start();
    }

    public function _failed(\Codeception\TestInterface $test, $fail)
    {
        $this->saveVideos[] = $this->getTestVideoFilename($test);
    }

    public function _after(\Codeception\TestInterface $test)
    {
        $this->recorder->signal(SIGKILL);
        $this->recorder->stop(0);


        if (!in_array($this->getTestVideoFilename($test), $this->saveVideos)) {
            $this->debug("Deleting ".$this->getTestVideoFilename($test));
            unlink($this->getTestVideoFilename($test));

            // test is fine so we exit
            return;
        }
        $this->debug('Convert video to mp4');
        // do something with video
        $this->convertToMp4($test);
    }


    public function _beforeStep(\Codeception\Step $step)
    {
        usleep(700000);
    }

    public function _afterStep(\Codeception\Step $step)
    {
        usleep(500000);
    }

    private function getCommand(\Codeception\TestInterface $test)
    {
        $vnc_host = getenv('VNC_HOST') ?: 'localhost';
        $vnc_port = getenv('VNC_PORT') ?: '5900';
        $vnc_pwdfile = getenv('VNC_PWDFILE') ?: __DIR__.'/../../vncpwd';

        $template = 'exec flvrec.py -o %s -P %s %s %d';

        return sprintf(
            $template,
            $this->getTestVideoFilename($test),
            $vnc_pwdfile,
            $vnc_host,
            $vnc_port
        );
    }

    private function getTestVideoFilename(\Codeception\TestInterface $test)
    {
        return sprintf(__DIR__.'/../../%s.flv', $test->getMetadata()->getName());
    }

    private function convertToMp4(\Codeception\TestInterface $test)
    {
        $command = 'ffmpeg -i %s -c:v libx264 -crf 19 -strict experimental %s';
        $outputFilename = sprintf(__DIR__.'/../../_output/%s.mp4', $test->getMetadata()->getName());

        // convert and move to _output dir
        $process = new Process(sprintf($command, $this->getTestVideoFilename($test), $outputFilename));

        $process->run();

        // remove source file
        unlink($this->getTestVideoFilename($test));

        return $outputFilename;
    }

}
