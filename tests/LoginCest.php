<?php

use Symfony\Component\Process\Process;

class LoginCest
{
    private $recorder;

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/');
    }


    public function canSeeProductsTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->canSee('Spanish inquisition');
    }

    public function canGoToLoginPage(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('Login');
        $I->seeInCurrentUrl('/login');
        $I->see('Login with your account');

    }

    public function _after(AcceptanceTester $I)
    {
        $I->amOnPage('/');
    }


}
