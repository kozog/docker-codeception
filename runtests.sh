#! /bin/bash

export VNC_HOST=127.0.0.1
export VNC_PORT=5900
export VNC_PASSWD=`pwd`/vncpwd

# let's make sure deps are installed
composer install

# Run docker container with selenium

docker run --net=host -d --name selenium_tester selenium/standalone-chrome-debug 

# lets run application and load fixtures

cd ./elcodi-docker
docker-compose up -d 
sleep 10 # let's wait 10 seconds for docker to startup
docker-compose exec elcodi app/console doctrine:fixtures:load --help

cd ..

./vendor/bin/codecept run --debug

# let's stop application

cd ./elcodi-docker
docker-compose stop

# let's stop the selenium container
cd ..
docker stop selenium_tester
docker rm selenium_tester


# Other helpful commands
# Build application container
# docker build --build-arg USER_ID=`id -u` -t elcodi_docker .
# recreate contaniers in compose
# docker-compose down 
# create new tests in codeception
# vendor/bin/codecept generate:cept acceptance register
