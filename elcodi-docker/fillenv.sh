#!/bin/bash

echo "Current User ID is `id -u`. Adding into to .env file"
cat ./.env.dist | sed "s/USERID/`id -u`/g" > .env
